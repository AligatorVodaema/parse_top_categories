import requests
from bs4 import BeautifulSoup
import json
from parse_top_films import parse

with open('index.html', encoding='utf-8') as file:
    feed = file.read()

soup = BeautifulSoup(feed, 'lxml')
list_of_tops = soup.find('div', class_="block_contact_us")
li_list = list_of_tops.find_all('li')

top_links = []
top_name_of_category = []
for li in li_list[:-1]:
    top_link = li.find('a').get('href')
    name_of_list = li.find('a').find('b').text
    top_links.append('http://the-cinema.cx' + top_link)
    top_name_of_category.append(name_of_list)

complete_json = []
for html_file in range(5):
    # with open(f'html_for_parse1\index{html_file}.html', encoding='utf-8') as file:
    #     new_feed = file.read()
    #
    # soup = BeautifulSoup(feed, 'lxml')
    # heading_items = soup.find_all('h2', class_="heading")
    #
    # films_list = []
    # for item in heading_items:
    #     numbers_of_films = item.next_element.strip()
    #     link = item.find('a').get('href')
    #     name = item.next_element.next_element.next_element.strip()
    #     film_dict = {
    #         numbers_of_films + ' ' + name: link
    #     }
    #     films_list.append(film_dict)
    films_list = parse(html_file)

    complete_json.append({
        top_name_of_category[html_file]: films_list
    })

with open(f'RESULT.json', 'a', encoding='utf-8') as jfile:
    json.dump(complete_json, jfile, indent=4, ensure_ascii=False)





#grab html pages
# countiter = 0
# for link in links:
#
#     url = f'{link}'
#
#     req = requests.get(url)
#     result = req.text
#
#     with open(f'html_for_parse1\index{countiter}.html', 'w', encoding='utf-8') as file:
#         file.write(result)
#     countiter += 1