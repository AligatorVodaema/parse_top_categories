import requests
from bs4 import BeautifulSoup
import json

# url = r'http://the-cinema.cx/top-100-films.html'
#
# req = requests.get(url)
# result = req.text
#
# with open('index.html', 'w', encoding='utf-8') as file:
#     file.write(result)


def parse(index: int) -> list:
    with open(f'html_for_parse1\\index{index}.html', encoding='utf-8') as file:
        feed = file.read()

    soup = BeautifulSoup(feed, 'lxml')
    heading_items = soup.find_all('h2', class_="heading")

    films_list = []

    for item in heading_items:
        numbers_of_films = item.next_element.strip()
        link = item.find('a').get('href')
        name = item.next_element.next_element.next_element.strip()
        film_dict = {
            numbers_of_films + ' ' + name: link
        }
        films_list.append(film_dict)
    return films_list

# with open('100films.json', 'w', encoding='utf-8') as file:
#     json.dump(films_list, file, indent=4, ensure_ascii=False)






